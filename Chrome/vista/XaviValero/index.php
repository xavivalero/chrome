<?php
session_start();
require_once("../db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="menu.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
    <script src="jquery.min.js"></script>
</head>
<body>
<div class="flex-container animate__animated animate__slideInDown">
    <!--Contenidor per a la destral-->
    <div class="flex-icono">
        <a href="../home.php">
            <img src="../imagenes/logo.png" alt="icono" id="icono">
        </a>
    </div>
    <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
    <div class="flex-item animate__animated animate__rubberBand">
        <span>T</span>
        <span>U</span>
        <span>T</span>
        <span>O</span>
        <span>R</span>
        <span>I</span>
        <span>A</span>
        <span>L</span>
        <span>E</span>
        <span>S</span>
    </div>
    <!--Contenidor per a la imatge de perfil -->
    <div class="flex-perfil">
        <a href="../log_in/login.html">
            <?php
            if(ISSET($_SESSION['NOMBRE'])) {
                echo "<img src='../imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
            }else {
                echo "<img src='../imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
            }
            ?>
        </a>
        <?php
        if(ISSET($_SESSION['NOMBRE'])) {
            echo "<form method='get' action='../../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form>";
        }
        ?>
    </div>
</div>
<div class="fondoVideos">
    <nav>
        <ul class="mcd-menu">
            <li>
                <a href="" onclick="cambiarSession('todo')">
                    <i class="fa fa-home"></i>
                    <strong>Todo</strong>
                    <small>sweet home</small>
                </a>
            </li>
            <li>
                <a href="" onclick=cambiarSession('web')>
                    <i class="fa fa-edit"></i>
                    <strong>Nuestra Web</strong>
                    <small>sweet home</small>
                </a>
            </li>
            <li>
                <a href="#" onclick=cambiarSession('tutoriales')>
                    <i class="fa fa-gift"></i>
                    <strong>Tutoriales</strong>
                    <small>sweet home</small>
                </a>
                <ul>
                    <li><a href="#" onclick=cambiarSession('fuego')><i class="fa fa-female"></i>Fuego</a></li>
                    <li><a href="#" onclick=cambiarSession('caza')><i class="fa fa-female"></i>Caza</a></li>
                </ul>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-globe"></i>
                    <strong>News</strong>
                    <small>sweet home</small>
                </a>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-comments-o"></i>
                    <strong>Blog</strong>
                    <small>what they say</small>
                </a>
                <ul>
                    <li><a href="#"><i class="fa fa-globe"></i>Mission</a></li>
                    <li>
                        <a href="#"><i class="fa fa-group"></i>Our Team</a>
                        <ul>
                            <li><a href="#"><i class="fa fa-female"></i>Leyla Sparks</a></li>
                            <li>
                                <a href="#"><i class="fa fa-male"></i>Gleb Ismailov</a>
                                <ul>
                                    <li><a href="#"><i class="fa fa-leaf"></i>About</a></li>
                                    <li><a href="#"><i class="fa fa-tasks"></i>Skills</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-female"></i>Viktoria Gibbers</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-trophy"></i>Rewards</a></li>
                    <li><a href="#"><i class="fa fa-certificate"></i>Certificates</a></li>
                </ul>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-picture-o"></i>
                    <strong>Portfolio</strong>
                    <small>sweet home</small>
                </a>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-envelope-o"></i>
                    <strong>Contacts</strong>
                    <small>drop a line</small>
                </a>
            </li>
        </ul>
    </nav>
    <div class="backgrVideos" id="backgrVideos">
        <a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a>
        <?php
        echo '<div class="title"><h2>todo</h2></div>';
        /** @var TYPE_NAME $conn */
        $query = $conn->query("SELECT * FROM tutoriales");


        foreach ($query as $valores):
            $video = $valores["video"];
            $titulo = $valores["titulo"];
            $descripcion = $valores["descripcion"];
            echo "<div class='videos animate__animated animate__slideInLeft'><video class='reproduce' width='200' controls>".
                "                            <source src='videos/$video.mp4' type='video/mp4'>".
                "                        </video>".
                "                       <div class='texto'>".
                "                            <h1>$titulo</h1>".
                "                               <p>$descripcion</p>".
                "                        </div></div>";
            endforeach;
        ?>
    </div>
</div>
<footer>
    <div><img src="imagenes/firma.png"></div>
    <div><p>© 2022 - Todos los derechos reservados(Xavi Valero Calderón)</p></div>
</footer>
<script>

        function cambiarSession(tipo) {

            var uno = document.getElementById('backgrVideos');
            if ( uno.hasChildNodes() )
            {
                while ( uno.childNodes.length >= 1 )
                {
                    uno.removeChild( uno.firstChild );
                }
            }

            var fondo = document.getElementById('backgrVideos');
            fondo.innerHTML = '<a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a><div class="title"><h2>'+tipo+'</h2></div>';

            var dataf = {'tipo' : tipo};

            $.ajax({
                type: "GET",
                url: "selectVideos.php",
                data: dataf,
                dataType: "json",
                encode: true,
            }).done(function (data) {

                let tituloData=1;
                let descripcionData=2;
                let videoData = 3;
                for (i=1;i<=Math.floor(Object.keys(data).length/3);i++) {
                    var video = data[videoData];
                    var titulo = data[tituloData];
                    var descripcion = data[descripcionData];

                    var fondo = document.getElementById('backgrVideos');
                    var nuevo = document.createElement('div');
                    nuevo.className = 'videos animate__animated animate__slideInLeft';
                    nuevo.innerHTML = '<video class="reproduce" width="200" controls>' +
                        '                            <source src="videos/' + video + '.mp4" type="video/mp4">' +
                        '                        </video>' +
                        '                        <div class="texto">' +
                        '                            <h1>' + titulo + '</h1>' +
                        '                               <p>' + descripcion + '</p>' +
                        '                        </div>'
                    fondo.appendChild(nuevo);

                    tituloData+=3;
                    descripcionData+=3;
                    videoData+= 3;
                }

            });

            event.preventDefault();

            atras.addEventListener('click', () => {
                atras.classList.remove('atras');
                setTimeout(() => atras.classList.add('atras'), 100);
                setTimeout(function(){
                    window.location="../home.php";
                },500);
            })

        }


        atras.addEventListener('click', () => {
            atras.classList.remove('atras');
            setTimeout(() => atras.classList.add('atras'), 100);
            setTimeout(function(){
            window.location="../home.php";
        },500);
        })

</script>
</body>
</html>