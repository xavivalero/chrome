<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/background.css">
    <link rel="stylesheet" href="style/fire.css">
    <link rel="stylesheet" href="./style/loader.css">
    <link rel="stylesheet" href="./style/tooltip.css">
    <link
       rel="stylesheet"
       href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
</head>
<body>
<!--Contenidor horitzontal -->
<div class="body-container">
    <div class="flex-container animate__animated animate__slideInDown">
        <!--Contenidor per a la destral-->
        <div class="flex-icono">
            <div class="tooltip logo">
                <img src="imagenes/logo.png" alt="icono" id="icono">
                <span class="tiptext">Reconocer antes que recordar</span>
            </div>
        </div>
        <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
        <div class="flex-item animate__animated animate__rubberBand tooltip titulo">
            <span>C</span>
            <span>H</span>
            <span>R</span>
            <span>O</span>
            <span>M</span>
            <span>E</span>
            <span class="tiptext">Relación entre el sistema y el mundo real</span>
        </div>
        <!--Contenidor per a la imatge de perfil -->
        <div class="flex-perfil">
            <a href="./log_in/login.html">
                <?php
                if(ISSET($_SESSION['NOMBRE'])) {
                    echo "<img src='imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
                }else {
                    echo "<img src='imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
                }
                ?>
            </a>
            <?php
            if(ISSET($_SESSION['NOMBRE'])) {
                echo "<div class='dropdown-content'><form method='get' action='../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form></div>";
            }
            ?>
        </div>
    </div>


        <div class="circle-menu tooltip menu animate__animated animate__slideInUp">
            <ul>
                <?php
                if(ISSET($_SESSION['NOMBRE'])) {
                    if ($_SESSION['NOMBRE'] == 'Victor') {
                        echo "<li><a class='noticias' href='pages/victorPortal/transiccion.html'><p>Portales</p></a></li>";
                    }elseif ($_SESSION['NOMBRE'] == 'Xavi'){
                        echo "<li><a class='noticias' href='XaviValero/loader.php'><p>Portales</p></a></li>";
                    }elseif ($_SESSION['NOMBRE'] == 'Marc'){
                        echo "<li><a class='noticias' href='pages/omiri/loader.html'><p>Portales</p></a></li>";
                    }else{
                        echo "<li><a class='noticias' href='#'><p>Portales</p></a></li>";
                    }
                }else{
                    echo "<li><a class='noticias' href='#'><p>Portales</p></a></li>";
                }
                ?>
                <li><a class="tutoriales" href="pages/getUsuarios.php"><p>Usuarios</p></a></li>
                <li><a class="tribus" href="pages/PaginaNoUsable.php"><p>No Usable</p></a></li>
                <li><a class="saldo" href="pages/results.php"><p>Resultados</p></a></li>
                <li><a class="trabajo" href="pages/trabajo.php"><p>Trabajo</p></a></li>
                <li><a class="inventario" href="pages/pagePersonalAccess.php"><p>Personal</p></a></li>
                <li><a class="tienda" href="pages/error404.html"><p>Error</p></a></li>
            </ul>
            <div class="fire">
                <div class="fire-left">
                    <div class="main-fire"></div>
                    <div class="particle-fire"></div>
                </div>
                <div class="fire-main">
                    <div class="main-fire"></div>
                    <div class="particle-fire"></div>
                </div>
                <div class="fire-right">
                    <div class="main-fire"></div>
                    <div class="particle-fire"></div>
                </div>
                <div class="fire-bottom">
                    <div class="main-fire"></div>
                </div>
            </div>
            <span class="tiptext">Centrar la atención en la actividad principal</span>
        </div>


    <div class="eventos">
        <div class="eventos--inner">
            <?php
            if (isset($_SESSION['cargarDatos'])) {
                require_once("db.php");

                /** @var TYPE_NAME $conn */
                $query = $conn->query("SELECT usuarios.id,nombre,mail,edad,descripcion FROM usuarios JOIN cargos WHERE usuarios.id_cargo = cargos.id ORDER BY usuarios.id");
                $imprimir = '';
                foreach ($query as $valores):
                    $id = $valores["id"];
                    $nombre = $valores["nombre"];
                    $mail = $valores["mail"];
                    $edad = $valores["edad"];
                    $cargo = $valores["descripcion"];
                    $imprimir = $imprimir . "$id | $nombre | $mail | $edad | $cargo --- ";
                endforeach;
                echo '<marquee scrollamount="20"  onmouseout="this.start()" onmouseover="this.stop()">' . $imprimir . '</marquee>';
                $_SESSION['cargarDatos'] = null;
            }
            ?>
        </div>
    </div>

    <div class="flex-footer">
        <h1>Contáctanos</h1>
          <button type='button'><a href='../controlador/cargarDatos.php'>Cargar Datos</a></button>


        <p>© 2021 - Todos los derechos reservados(Víctor, Marc, Xavi)<a class="guiEstils" href="pages/guiaEstils.html">Guia d'estils</a></p>
    </div>
</div>

</body>
</html>