<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos Tabular</title>
    <link rel="stylesheet" href="../style/table-grid.css">
    <link rel="stylesheet" href="./style/tooltip.css">
</head>
<body>
<a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a>
<div class="center-div">
    <form action="getUsuarios.php" method="POST">
        <p>filtrar por:</p>
        <select id="rol" name="rol">
            <option value="recolector">Recolector</option>
            <option value="cazador">Cazador</option>
            <option value="guerrero">Guerrero</option>
            <option value="pescador">Pescador</option>
            <option value="jefe">Jefe</option>
        </select>
        <p>ordenar por:</p>
        <select id="order" name="order">
            <option value="id">Id</option>
            <option value="nombre">Nombre</option>
            <option value="mail">Mail</option>
            <option value="edad">Edad</option>
            <option value="descripcion">Cargo</option>
        </select>
        <input type="submit" value="Buscar">
    </form>
</div>
<div class="table">
    <div class="table-row">
        <div class="table-head">ID</div>
        <div class="table-head">NOMBRE</div>
        <div class="table-head">MAIL</div>
        <div class="table-head">EDAD</div>
        <div class="table-head">CARGO</div>
    </div>
    <?php
    $query = null;
    require_once("../db.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['rol']) && isset($_POST['order'])) {
            $order = $_POST['order'];
            $rol = $_POST['rol'];
            /** @var TYPE_NAME $conn */
            $query = $conn->query("SELECT u.id,u.nombre ,u.mail ,u.edad ,c.descripcion FROM usuarios u JOIN cargos c on u.id_cargo = c.id WHERE c.descripcion = '$rol' ORDER BY $order ;");
        }
    } else {
        /** @var TYPE_NAME $conn */
        $query = $conn->query("SELECT usuarios.id,nombre,mail,edad,descripcion FROM usuarios JOIN cargos WHERE usuarios.id_cargo = cargos.id");
    }


    foreach ($query as $valores):
        $id = $valores["id"];
        $nombre = $valores["nombre"];
        $mail = $valores["mail"];
        $edad = $valores["edad"];
        $cargo = $valores["descripcion"];
        echo '<div class="table-row"> 
                  <div class="table-cell">' . $id . '</div> 
                  <div class="table-cell">' . $nombre . '</div> 
                  <div class="table-cell">' . $mail . '</div> 
                  <div class="table-cell">' . $edad . '</div> 
                  <div class="table-cell">' . $cargo . '</div> 
              </div>';
    endforeach;
    ?>
</div>

<script>
    atras.addEventListener('click', () => {
        atras.classList.remove('atras');
        setTimeout(() => atras.classList.add('atras'), 100);
        setTimeout(function(){
            window.location="../home.php";
        },500);
    })

</script>

</body>
</html>