<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Personal Access</title>
    <link rel="stylesheet" href="./style/tooltip.css">
    <link rel="stylesheet" href="../style/personalAccessStyle.css">
    <link rel="stylesheet" href="../style/caraboba.css">
    <link rel="stylesheet" href="../style/hojas.css">
    <link rel="stylesheet" href="../style/snow.css">
    <link rel="stylesheet" href="../style/check.css">
    <link rel="stylesheet" href="../style/tooltip.css">
    <script src="../javascript/caraboba.js"></script>
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        var nbDrop = 400;

        function randRange(maxNum, minNum) {
            return (Math.floor(Math.random(10) * (maxNum - minNum + 1)) + minNum);
        }

        function createRain() {
            for (i = 0; i < nbDrop; i++) {

                var dropLeft = randRange(0, 3000);
                var dropTop = randRange(-1000, 1000);
                $('.rain').append('<div class="drop" id="drop'+ i +'"></div>');
                $('#drop' + i).css('left', dropLeft);
                $('#drop' + i).css('top', dropTop);
            }
        }
    </script>
</head>
<body id="body">
<a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a>
<section id="rain" class="rain" style="display: none"></section>
<div class="welcome">
    <?php
    if (isset($_SESSION['NOMBRE'])) {
        $nombre = $_SESSION['NOMBRE'];
        echo "<h2>Bienvenido</h2> <h2 id='nombre'>$nombre</h2>";
    } else {
        echo "<h2 id='nombre'></h2>";
    }
    ?>
</div>
<div class="hojas" id="hojas" style="display: none">
    <div id="leaves">
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
    </div>
</div>

<div class="snowflakes" id="snowflakes" aria-hidden="true" style="display: none">
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❄
    </div>
</div>
<div class="modo animate__animated animate__slideInDown" id="modo">
    <div class="tooltip checkbox" style="margin-left: 5%">
        <input class="check" id="check" onclick="personalizar()" type="checkbox" style="margin-left: 5%">
        <span class="tiptext">Constancia y estándares</span>
    </div>
    <p class="escModo" id="escModo">modo utilizacion</p>
</div>
<div class="personalizacion animate__animated animate__slideInUp" id="personalizacion">
    <div class="contingente">
        <label for="titulo">Titulo: </label>
        <input type="text" maxlength="11" id="titulo">
        <label for="color" style="margin-left: 20px">Color: </label>
        <input type="color" id="colorEscoger" value="#f45e61">
        <label for="fuente" style="margin-left: 20px">Fuente: </label>
        <div class="select">
            <select id="escFuente" name="escFuente">
                <option value="fuente1" class="fuente1">EJEMPLO</option>
                <option value="fuente2" class="fuente2">EJEMPLO</option>
                <option value="fuente3" class="fuente3">EJEMPLO</option>
            </select>
        </div>
        <label for="posicion" style="margin-left: 20px">Posición: </label>
        <div class="select">
            <select id="escPosicion" name="escPosicion">
                <option value="dos">Pos. 2</option>
                <option value="tres">Pos. 3</option>
            </select>
        </div>
        <button type="button" class="buttonform ripple" onclick="titulo()">Aplicar</button>
    </div>
    <div class="contingente">
        <p style="margin-right: 10px">Imagen:</p>
        <div class="select">
            <select id="escImagen" name="escImagen">
                <option value="fondo1">Lago helado</option>
                <option value="fondo2">Atardecer</option>
                <option value="fondo3">Cueva</option>
                <option value="fondo4">Cielo de otoño</option>
            </select>
        </div>
        <button class="buttonform ripple" onclick="aplicarFondo()">Aplicar</button>
        <div class="cara">
            <label for="posicion" style="margin-right: 10px">Cara: </label>
            <div class="select">
                <select id="escCara" name="escCara">
                    <option value="si">SI</option>
                    <option value="no">NO</option>
                </select>
            </div>
            <button style="margin-left: 10px" class="buttonform ripple" onclick="activarCara()">Aplicar</button>
        </div>
        <div class="caer">
            <label for="posicion" style="margin-right: 10px">Ambientación: </label>
            <div class="select">
                <select id="escCaer" name="escCaer">
                    <option value="hojas">Hojas</option>
                    <option value="nieve">Nieve</option>
                    <option value="lluvia">Lluvia</option>
                </select>
            </div>
            <button style="margin-left: 10px" class="btnCaer buttonform ripple" onclick="caer()">Aplicar</button>
        </div>
        <p style="margin-right: 10px">Slider Estaciones:</p>
        <div class="select">
            <select id="escPosicionEstaciones" name="escPosicionEstaciones">

                <option value="uno">Pos. 1</option>
                <option value="cuatro">Pos. 4</option>
                <option value="cinco">Pos. 5</option>
                <option value="seis">Pos. 6</option>
                <option value="siete">Pos. 7</option>
                <option value="ocho">Pos. 8</option>
                <option value="nueve">Pos. 9</option>
            </select>
        </div>
        <button style="margin-left: 10px" class="btnEstaciones buttonform ripple" id="btnEstaciones"
                onclick="mostrarEstaciones()">Aplicar
        </button>
    </div>
    <div class="contingente">
        <label for="posicion" style="margin-right: 10px">Slider Noticies: </label>
        <div class="select">
            <select id="escNoticias" name="escNoticias">
                <option value="si">SI</option>
                <option value="no">NO</option>
            </select>
        </div>
        <label for="posicion" style="margin-left: 20px">Posició: </label>
        <input type="text" value="Pos. 3" disabled>

        <label for="contenido"  style="margin-left: 20px"> Contenido: </label>
        <input type="text" id="contenidoNoticias">
        <button style="margin-left: 10px" class="btnNoticias buttonform ripple" onclick="mostrarNoticias()">
            Aplicar
        </button>

        <label for="posicion"  style="margin-left: 20px">Constelaciones: </label>
        <div class="select">
            <select id="escPosicionConstelacion" name="escPosicionConstelacion">
                <option value="uno">Pos. 1</option>
                <option value="cuatro">Pos. 4</option>
                <option value="cinco">Pos. 5</option>
                <option value="seis">Pos. 6</option>
                <option value="siete">Pos. 7</option>
                <option value="ocho">Pos. 8</option>
                <option value="nueve">Pos. 9</option>
            </select>
        </div>
        <button style="margin-left: 10px" class="buttonform ripple" id="btnTiempo"
                onclick="mostrarConstelacion()">Aplicar
        </button>
    </div>

</div>
<div class="btnPersonalizar">
    <div class="tooltip btnGuardar">
        <button class="botonesArriba" id="btnGuardar" onclick="guardar()" style="display: none"><img src="../imagenes/jarron.png" height ="15px" width="20%"/>Guardar</button>
        <?php
            if(isset($_SESSION['NOMBRE'])){

            }else{
                echo '<span class="tiptext">Atención! Necesitas iniciar sesión para guardar tus cambios!</span>';
            }
        ?>
    </div>
    <button class="botonesArriba" id="btnRestaurar" onclick="restaurar()" style="display: none">Restaurar</button>
</div>
<div class="uno" id="uno" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="unop">UNO</p>
    <div id="container" draggable="true" ondragstart="drag(event)" style="display: none">
        <button class="face-button">
    <span class="face-container">
      <span class="eye left"></span>
      <span class="eye right"></span>
      <span class="mouth"></span>
    </span>
        </button>
    </div>
</div>
<div class="dos" id="dos" value="2" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="dosp">DOS</p>
</div>
<div class="tres" id="tres" value="3" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="tresp" >TRES</p>
</div>
<div class="cuatro" id="cuatro" value="4" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="cuatrop" >CUATRO</p>
</div>
<div class="cinco" id="cinco" value="5" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="cincop" >CINCO</p>
</div>
<div class="seis" id="seis" value="6" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="seisp" >SEIS</p>
</div>
<div class="siete" id="siete" value="7" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="sietep" >SIETE</p>
</div>
<div class="ocho" id="ocho" value="8" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="ochop" >OCHO</p>
</div>
<div class="nueve" id="nueve" value="9" ondrop="drop(event)" ondragover="allowDrop(event)">
    <p id="nuevep" >NUEVE</p>
</div>
<script src="../javascript/jsPersonalAccess.js"></script>
<script>
    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("Div", ev.target.id);
    }
    function drop(ev) {
        var data = ev.dataTransfer.getData("Div");
        ev.target.appendChild(document.getElementById(data));
        ev.preventDefault();
    }

    atras.addEventListener('click', () => {
        atras.classList.remove('atras');
        setTimeout(() => atras.classList.add('atras'), 100);
        setTimeout(function(){
            window.location="../home.php";
        },500);
    })
</script>
</body>
</html>