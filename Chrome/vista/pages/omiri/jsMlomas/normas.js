var imgs = [
        'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/3dec169c-79f7-4194-ac3d-53fd7c58565f/d7sgpwr-173403bd-64b3-4a4d-96d6-12ce8a92731c.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzNkZWMxNjljLTc5ZjctNDE5NC1hYzNkLTUzZmQ3YzU4NTY1ZlwvZDdzZ3B3ci0xNzM0MDNiZC02NGIzLTRhNGQtOTZkNi0xMmNlOGE5MjczMWMuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.Uuh5HBhlWO_cOLuscpTmggWnKOy_RRH1_8PgUpzqAzo',
        'https://gitlab.com/xavivalero/chrome/-/raw/main/Chrome/vista/pages/omiri/images/fogata.jpg',
        'https://wallpaperset.com/w/full/2/6/5/78381.jpg',
        'https://gitlab.com/xavivalero/chrome/-/raw/main/Chrome/vista/pages/omiri/images/mamut.jpg',
        'https://images.unsplash.com/photo-1506361797048-46a149213205?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1200&q=50'
    ],
    n = imgs.length,
    current = n-1,
    closedWidth = Math.floor(window.innerWidth/10)
var texto = [
    'Encuentra una cueva dónde dormir y esconderte por la noche o en caso de lluvias.',
    'Encender fuego es muy importante para cocinar y mantenerse caliente.',
    'Si vas solo caza solo animales inofensivos.',
    'Aunque seais un grupo no os metais con animales muy grandes.',
    ''
]

for (var i=0; i<n; i++){

    var bgImg = document.createElement('div');
    bg.appendChild(bgImg);

    gsap.set(bgImg, {
        attr:{id:'bgImg'+i, class:'bgImg'},
        width:'100%',
        height:'100%',
        backgroundImage:'url('+imgs[i]+')',
        backgroundSize:'cover',
        backgroundPosition:'center'
    })

    var b = document.createElement('div');
    fg.appendChild(b);

    gsap.fromTo(b, {
        attr:{id:'b'+i, class:'box'},
        innerHTML:'<p><sub>Norma:</sub> '+(i+1)+'.</p>',
        width:'100%',
        height:'100%',
        borderLeft:(i>0)?'solid 1px #eee':'',
        backgroundColor:'rgba(250,250,250,0)',
        left:i*closedWidth,
        transformOrigin:'100% 100%',
        x:'100%'
    },{
        duration:i*0.15,
        x:0,
        ease:'expo.inOut'
    })

    b.onmouseenter = b.onclick = (e)=>{
        if (Number(e.currentTarget.id.substr(1))==current) return;

        var staggerOrder = !!(current < Number(e.currentTarget.id.substr(1)));
        current = Number(e.currentTarget.id.substr(1));
        gsap.to('.box', {
            duration:0.5,
            ease:'elastic.out(0.3)',
            left:(i)=>(i<=current)? i*closedWidth: window.innerWidth-((n-i)*closedWidth),
            x:0,
            stagger: staggerOrder? 0.05:-0.05
        })

        bg.appendChild( document.getElementById('bgImg'+current) )
        gsap.fromTo('#bgImg'+current, {opacity:0}, {opacity:1, duration:0.3, ease:'power1.inOut'})
        gsap.fromTo('#bgImg'+current, {scale:1.05, rotation:0.05}, {scale:1, rotation:0, duration:1.5, ease:'sine'})
    }
}


window.onresize = (e)=>{
    closedWidth = Math.floor(window.innerWidth/10)
    gsap.set('.box', { x:0, left:(i)=> (i<=current)? i*closedWidth: window.innerWidth-((n-i)*closedWidth) })
}