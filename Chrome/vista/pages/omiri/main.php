<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Resultados</title>
    <link rel="stylesheet" href="cssMlomas/footer.css">
    <link rel="stylesheet" href="./cssMlomas/main.css">
    <link rel="stylesheet" href="./cssMlomas/card.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
</head>
<body>
<div class="flex-container animate__animated animate__slideInDown">
    <!--Contenidor per a la destral-->
    <div class="flex-icono">
        <a href="../../home.php">
            <img src="../../imagenes/logo.png" alt="icono" id="icono">
        </a>
    </div>
    <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
    <div class="flex-item animate__animated animate__rubberBand">
        <span>I</span>
        <span>N</span>
        <span>V</span>
        <span>E</span>
        <span>S</span>
        <span>T</span>
        <span>I</span>
        <span>G</span>
        <span>A</span>
    </div>
    <!--Contenidor per a la imatge de perfil -->
    <div class="flex-perfil">
        <a href="../../log_in/login.html">
            <?php
            if (isset($_SESSION['NOMBRE'])) {
                echo "<img src='../../imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
            } else {
                echo "<img src='../../imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
            }
            ?>
        </a>
        <?php
        if (isset($_SESSION['NOMBRE'])) {
            echo "<form method='get' action='../../../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form>";
        }
        ?>
    </div>
</div>
<div class="page-content">
    <div class="card animate__animated animate__slideInLeft">
        <div class="content">
            <h2 class="title">Vegetación</h2>
            <p class="copy">Descubre el peligro y las ventajas de las plantas que te rodean.</p>
            <button class="btn" id="plantas">INVESTIGA</button>
        </div>
    </div>
    <div class="card animate__animated animate__slideInLeft">
        <div class="content">
            <h2 class="title">Animales</h2>
            <p class="copy">Aprende que animales son peligrosos y cuáles son fáciles de matar.</p>
            <button class="btn" id="animales">INVESTIGA</button>
        </div>
    </div>
    <div class="card animate__animated animate__slideInRight">
        <div class="content">
            <h2 class="title">Normas</h2>
            <p class="copy">Que pasos seguir para ser un buen investigador?</p>
            <button class="btn" id="normas">INVESTIGA</button>
        </div>
    </div>
    <div class="card animate__animated animate__slideInRight">
        <div class="content">
            <h2 class="title">Orientación</h2>
            <p class="copy">Observa las estrellas que te rodean para saber el camino de vuelta.</p>
            <button class="btn" id="orientacion">INVESTIGA</button>
        </div>
    </div>
</div>
<div class="footer-basic">
    <footer>
        <div class="social"><a href="../error404.html"><i class="icon ion-android-happy"></i></a><a href="../error404.html"><i class="icon ion-leaf"></i></a><a href="../error404.html"><i class="icon ion-social-twitter"></i></a><a href="../error404.html"><i class="icon ion-social-octocat"></i></a></div>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="../../home.php">Home</a></li>
            <li class="list-inline-item"><a href="../../log_in/login.html">Register/Log in</a></li>
            <li class="list-inline-item"><a href="#">About</a></li>
            <li class="list-inline-item"><a href="../guiaEstils.html">Guia</a></li>
            <li class="list-inline-item"><a href="../error404.html">Privacy Policy</a></li>
        </ul>
        <p class="copyright">FreeUrant © 2024</p>
        <div class="firma"><img src="images/firma.png"></div>
    </footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
</body>
<script>
    plantas.addEventListener('click', () => {
        window.location="./pruebas/plantas.html";
    })

    animales.addEventListener('click', () => {
        window.location="./pruebas/animales.html";
    })

    normas.addEventListener('click', () => {
        window.location="./pruebas/normas.html";
    })

    orientacion.addEventListener('click', () => {
        window.location="./pruebas/star.html";
    })
</script>
</html>

