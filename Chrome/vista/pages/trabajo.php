<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabajo</title>
    <link rel="stylesheet" href="../style/styleTrabajo.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
</head>
<body>
<div class="flex-container animate__animated animate__slideInDown">
    <!--Contenidor per a la destral-->
    <div class="flex-icono">
        <a href="../home.php">
        <img src="../imagenes/logo.png" alt="icono" id="icono">
        </a>
    </div>
    <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
    <div class="flex-item animate__animated animate__rubberBand">
        <span>T</span>
        <span>R</span>
        <span>A</span>
        <span>B</span>
        <span>A</span>
        <span>J</span>
        <span>O</span>
    </div>
    <!--Contenidor per a la imatge de perfil -->
    <div class="flex-perfil">
        <a href="../log_in/login.html">
            <?php
            if(ISSET($_SESSION['NOMBRE'])) {
                echo "<img src='../imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
            }else {
                echo "<img src='../imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
            }
            ?>
        </a>
        <?php
        if(ISSET($_SESSION['NOMBRE'])) {
            echo "<form method='get' action='../../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form>";
        }
        ?>
    </div>
</div>

<div class="segundoFondo">
    <a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a>
    <div class="welcome">
        <?php
        if (isset($_SESSION['NOMBRE'])) {
            $nombre = $_SESSION['NOMBRE'];
            $foto = $_SESSION['foto'];
            echo "<h2> bienvenido $nombre</h2> <img class='imageRol' src='../imagenes/$foto' alt=''>";
        }
        ?>
    </div>
    <?php
    if (isset($_SESSION['NOMBRE'])) {
        $nombre = $_SESSION['NOMBRE'];
        echo "<form method='get' action='../../controlador/selectTrabajadores.php'>";
        echo "<button type='submit' class='btn'>Click</button>";
        echo "</form>";
        if (isset($_SESSION['trabajadores'])) {
            $listaTrabaj = $_SESSION['trabajadores'];
            echo "<div class='table'>";
            echo "<div class='table-row'><div class='table-head'><strong>id</strong></div><div class='table-head'><strong>nombre</strong></div><div class='table-head'><strong>edad</strong></div><div class='table-head'><strong>mail</strong></div><div class='table-head'><strong>contraseña</strong></div><div class='table-head'><strong>descripcion</strong></div></div>";
            foreach ($listaTrabaj as $value) {
                echo "<div class='table-row'><div class='table-cell'>" . $value['id'] . "</div><div class='table-cell'>" . $value['nombre'] . "</div><div class='table-cell'>" . $value['edad'] . "</div><div class='table-cell'>" . $value['mail'] . "</div><div class='table-cell'>" . $value['contraseña'] . "</div><div class='table-cell'>" . $value['descripcion'] . "</div></div>";
            }
            echo "</div>";

            $_SESSION['trabajadores'] = null;
        }
    }
    ?>
    <?php
    if(ISSET($_SESSION['idCargo'])) {
        if ($_SESSION['idCargo'] == 5) {
            echo "<div class='diagrama'>
        <h3>Porcentaje de cada tipo de trabajador/rol</h3>
        <canvas id='myChart' width='10' height='10'></canvas>
        </div>
        <div class='diagrama2'>
        <h3>Edad de la población</h3>
        <canvas id='barChart' width='10' height='10'></canvas>
    </div>";
        }
    }
    ?>
</div>
<script>
    atras.addEventListener('click', () => {
        atras.classList.remove('atras');
        setTimeout(() => atras.classList.add('atras'), 100);
        setTimeout(function(){
            window.location="../home.php";
        },500);
    })

</script>
</body>
<script src="../javascript/charRoles.js"></script>
</html>
