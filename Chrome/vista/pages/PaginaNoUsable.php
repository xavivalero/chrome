<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil</title>
    <link rel="stylesheet" href="../style/styleNoUsable.css">
    <link rel="stylesheet" href="../style/background.css">
    <link rel="stylesheet" href="../style/fire.css">
    <link rel="stylesheet" href="../style/loader.css">
    <link rel="stylesheet" href="../style/tooltip.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
</head>
<body>
<!--Contenidor horitzontal -->
<div class="body-container">
    <div class="flex-container animate__animated animate__slideInDown">
        <!--Contenidor per a la destral-->
        <div class="flex-icono">
            <div class="tooltip logo">
                <img src="../imagenes/piedraNoUsable.png" alt="icono" id="icono">
                <span class="tiptext"> Adecuación entre el sistema y el mundo real.
                    Una persona no relaciona el concepto de una piedra con la home </span>
            </div>
        </div>
        <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
        <div class="flex-item animate__animated animate__rubberBand tooltip titulo">
            <span>C</span>
            <span>H</span>
            <span>R</span>
            <span>O</span>
            <span>M</span>
            <span>E</span>
            <span class="tiptext">Consistencia y estándares. No cumple con el equilibrio y le da una sensación futurista</span>
        </div>
        <!--Contenidor per a la imatge de perfil -->
        <div class="flex-perfil">
            <a href="./log_in/login.html">
                <?php
                if (isset($_SESSION['NOMBRE'])) {
                    echo "<img src='../imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
                } else {
                    echo "<img src='../imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
                }
                ?>
            </a>
            <?php
            if (isset($_SESSION['NOMBRE'])) {
                echo "<div class='dropdown-content'><form method='get' action='../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form></div>";
            }
            ?>
        </div>
    </div>


    <div class="circle-menu tooltip menu animate__animated animate__slideInUp">
        <ul>
            <li><a class="noticias" href="#"><p>Notícias</p></a></li>
            <li><a class="tutoriales" href="pages/getUsuarios.php"><p>Tutoriales</p></a></li>
            <li><a class="tribus" href="pages/PaginaNoUsable.html"><p>Tribus</p></a></li>
            <li><a class="saldo" href="#"><p>Saldo</p></a></li>
            <li><a class="trabajo" href="pages/trabajo.php"><p>Trabajo</p></a></li>
            <li><a class="inventario" href="pages/pagePersonalAccess.php"><p>Personal</p></a></li>
            <li><a class="tienda" href="pages/error404.html"><p>Tienda</p></a></li>
        </ul>
        <div class="fire">
            <div class="fire-left">
                <div class="main-fire"></div>
                <div class="particle-fire"></div>
            </div>
            <div class="fire-main">
                <div class="main-fire"></div>
                <div class="particle-fire"></div>
            </div>
            <div class="fire-right">
                <div class="main-fire"></div>
                <div class="particle-fire"></div>
            </div>
            <div class="fire-bottom">
                <div class="main-fire"></div>
            </div>
        </div>
        <span class="tiptext"> Ayudar a los usuarios a reconocer, diagnosticar y solucionar los errores. Cuando le das
        no te dice el porqué ha fallado y no te muestra ningún tipo de mensaje</span>
    </div>
    <div class="flex-estorbo">
        <div class="tooltip logo">
            <img src='../imagenes/estorbo.png' alt='perfil' class='perfilCavernicola'>
            <span class="tiptext">Estética y diseño minimalista. Una imagen que esta mal puesta y es grande que quita la importancia de lo demás</span>
        </div>
    </div>
    <div class="eventos">
        <div class="eventos--inner">
            <?php
            if (isset($_SESSION['cargarDatos'])) {
                require_once("db.php");

                /** @var TYPE_NAME $conn */
                $query = $conn->query("SELECT usuarios.id,nombre,mail,edad,descripcion FROM usuarios JOIN cargos WHERE usuarios.id_cargo = cargos.id ORDER BY usuarios.id");
                $imprimir = '';
                foreach ($query as $valores):
                    $id = $valores["id"];
                    $nombre = $valores["nombre"];
                    $mail = $valores["mail"];
                    $edad = $valores["edad"];
                    $cargo = $valores["descripcion"];
                    $imprimir = $imprimir . "$id | $nombre | $mail | $edad | $cargo --- ";
                endforeach;
                echo '<marquee scrollamount="20"  onmouseout="this.start()" onmouseover="this.stop()">' . $imprimir . '</marquee>';
                $_SESSION['cargarDatos'] = null;
            }
            ?>
        </div>
    </div>

    <div class="tooltip flex-footer">
        <h1>Contáctanos</h1>
        <button type='button'><a href='../../controlador/cargarDatos.php'>Cargar Datos</a></button>
        <p>© 2021 - Todos los derechos reservados(Víctor, Marc, Xavi)<a class="guiEstils" href="GuiaNoUsable.html">Guia
                d'estils</a></p>
        <span class="tiptext">Ayuda y documentación. Donde tendría que estar la guía de estilos para informarte sobre la página, no hay nada para ayudarte donde te redirige.</span>
    </div>
</div>

</body>
</html>