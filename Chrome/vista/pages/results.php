<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Resultados</title>
    <link rel="stylesheet" href="../style/resultados.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
</head>
<body>
<div class="flex-container animate__animated animate__slideInDown">
    <!--Contenidor per a la destral-->
    <div class="flex-icono">
        <a href="../home.php">
            <img src="../imagenes/logo.png" alt="icono" id="icono">
        </a>
    </div>
    <!--Títol fet amb span per a tractar l'animació de cada lletra per separat -->
    <div class="flex-item animate__animated animate__rubberBand">
        <span>R</span>
        <span>E</span>
        <span>S</span>
        <span>U</span>
        <span>L</span>
        <span>T</span>
        <span>S</span>
    </div>
    <!--Contenidor per a la imatge de perfil -->
    <div class="flex-perfil">
        <a href="../log_in/login.html">
            <?php
            if (isset($_SESSION['NOMBRE'])) {
                echo "<img src='../imagenes/cueva.png' alt='perfil' class='perfilCavernicola'>";
            } else {
                echo "<img src='../imagenes/cueva2.png' alt='perfil' class='perfilCavernicola'>";
            }
            ?>
        </a>
        <?php
        if (isset($_SESSION['NOMBRE'])) {
            echo "<form method='get' action='../../controlador/cerrarSesion.php'><button type='submit' class='btnCerrar'>Cerrar sesión</button></form>";
        }
        ?>
    </div>
</div>

<div class="segundoFondo">
    <a href="#"><img class="flecha" id="atras" alt="" src="../imagenes/flecha-blanca.png"></a>
    <div class="fluxmap">
        <h1 class="animate__animated animate__slideInLeft">FLUJO SITIO WEB</h1>
        <img class="animate__animated animate__slideInRight" id="" alt="" src="../imagenes/fluxmap.png">
    </div>

    <div class="tagcloud">
        <h1>RESULTADOS</h1>
        <p><strong>Para poder probar nuestro portal web, hemos hecho un test a tres usuarios, los cuales les hemos pedido
                que
                se registraran i que buscaran los trabajadores guerreros, i los ordenaran por nombre.
                El test no ha sido muy satisfactorio ya que no encontraban la cueva para registrar-se, dijeron que el
                menú te atrapaba mucho y por eso buscaban allí para hacerlo.
                <br>
                <br>
                Hay que decir que tampoco ayudaba que el tema de nuestro portal sea de hace tanto tiempo, ya que les dije
                que intentaran ponerse en la piel de un cromañon.
                <br>
                <br>
                La primera vez que realizaron el test tardaron algo más de un minuto en realizar las dos tareas. Pero luego ya entendieron que la cueva simboliza una casa
                para los cavernicolas.
                <br>
                <br>
                Como conculsión, pensamos que tendríamos que ponerle una pequeña animación para llamar la atención del usuario
                para registrar-se y cambiar los nombres de las entradas del menú para que sean más descriptivos.
            </strong></p>
        <img class="" id="" alt="" src="../imagenes/results.PNG">
        <h1>TAGCLOUD</h1>
        <img class="animate__animated animate__slideInRight" id="" alt="" src="../imagenes/tagcloud.png">
    </div>

</div>
<script>
    atras.addEventListener('click', () => {
        atras.classList.remove('atras');
        setTimeout(() => atras.classList.add('atras'), 100);
        setTimeout(function () {
            window.location = "../home.php";
        }, 500);
    })

</script>
</body>
</html>

