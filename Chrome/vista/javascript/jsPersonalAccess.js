var pers = true;
const indexEst = [0,0,0,0,0,0,0,0,0];

var pos = document.getElementById('escPosicionEstaciones').value;
var divParent = document.getElementById(pos);

var nombre = document.getElementById('nombre');
if(nombre.innerHTML == ''){
    //alert('Tienes que inciar sesión');
    document.getElementById('body').style.backgroundImage = "url('../imagenes/oscuro.jpg')";
}else{
    //alert('Hola ' + nombre.innerHTML);
    var body = document.getElementById('body');

    if(localStorage.getItem(nombre.innerHTML)) {
        body.innerHTML = localStorage.getItem(nombre.innerHTML);
        document.getElementById('body').style.backgroundImage = localStorage.getItem(nombre.innerHTML + 'Background');
    }
}

function personalizar() {
    if(pers == true) {
        var uno = document.getElementById('uno');
        var dos = document.getElementById('dos');
        var tres = document.getElementById('tres');
        var cuatro = document.getElementById('cuatro');
        var cinco = document.getElementById('cinco');
        var seis = document.getElementById('seis');
        var siete = document.getElementById('siete');
        var ocho = document.getElementById('ocho');
        var nueve = document.getElementById('nueve');
        var unop = document.getElementById('unop');
        var dosp = document.getElementById('dosp');
        var tresp = document.getElementById('tresp');
        var cuatrop = document.getElementById('cuatrop');
        var cincop = document.getElementById('cincop');
        var seisp = document.getElementById('seisp');
        var sietep = document.getElementById('sietep');
        var ochop = document.getElementById('ochop');
        var nuevep = document.getElementById('nuevep');

        uno.style.border = "2px solid #ea9352";
        dos.style.border = "2px solid #ea9352";
        tres.style.border = "2px solid #ea9352";
        cuatro.style.border = "2px solid #ea9352";
        cinco.style.border = "2px solid #ea9352";
        seis.style.border = "2px solid #ea9352";
        siete.style.border = "2px solid #ea9352";
        ocho.style.border = "2px solid #ea9352";
        nueve.style.border = "2px solid #ea9352";
        unop.style.visibility = "visible";
        dosp.style.visibility = "visible";
        tresp.style.visibility = "visible";
        cuatrop.style.visibility = "visible";
        cincop.style.visibility = "visible";
        seisp.style.visibility = "visible";
        sietep.style.visibility = "visible";
        ochop.style.visibility = "visible";
        nuevep.style.visibility = "visible";

        var btnGuardar = document.getElementById('btnGuardar');
        btnGuardar.style.display = 'flex';

        var btnRestaurar = document.getElementById('btnRestaurar');
        btnRestaurar.style.display = 'flex';

        document.getElementById('escModo').innerHTML = "modo edicion";

        var divPers = document.getElementById('personalizacion')
        divPers.style.display = "block";

        pers = false;
    }else{
        var uno = document.getElementById('uno');
        var dos = document.getElementById('dos');
        var tres = document.getElementById('tres');
        var cuatro = document.getElementById('cuatro');
        var cinco = document.getElementById('cinco');
        var seis = document.getElementById('seis');
        var siete = document.getElementById('siete');
        var ocho = document.getElementById('ocho');
        var nueve = document.getElementById('nueve');
        var unop = document.getElementById('unop');
        var dosp = document.getElementById('dosp');
        var tresp = document.getElementById('tresp');
        var cuatrop = document.getElementById('cuatrop');
        var cincop = document.getElementById('cincop');
        var seisp = document.getElementById('seisp');
        var sietep = document.getElementById('sietep');
        var ochop = document.getElementById('ochop');
        var nuevep = document.getElementById('nuevep');

        uno.style.border = "0px solid #ea9352";
        dos.style.border = "0px solid #ea9352";
        tres.style.border = "0px solid #ea9352";
        cuatro.style.border = "0px solid #ea9352";
        cinco.style.border = "0px solid #ea9352";
        seis.style.border = "0px solid #ea9352";
        siete.style.border = "0px solid #ea9352";
        ocho.style.border = "0px solid #ea9352";
        nueve.style.border = "0px solid #ea9352";
        unop.style.visibility = "hidden";
        dosp.style.visibility = "hidden";
        tresp.style.visibility = "hidden";
        cuatrop.style.visibility = "hidden";
        cincop.style.visibility = "hidden";
        seisp.style.visibility = "hidden";
        sietep.style.visibility = "hidden";
        ochop.style.visibility = "hidden";
        nuevep.style.visibility = "hidden";

        var btnSave = document.getElementById('btnGuardar');
        btnSave.style.display = 'none';

        var btnRestaurar = document.getElementById('btnRestaurar');
        btnRestaurar.style.display = 'none';

        document.getElementById('escModo').innerHTML = "modo utilizacion";

        var divPers = document.getElementById('personalizacion')
        divPers.style.display = "none";

        pers = true;
    }
}

function restaurar(){
    document.getElementById('body').style.backgroundImage = "url('../imagenes/oscuro.jpg')";
    document.getElementById('hojas').style.display = 'none';
    document.getElementById('rain').style.display = 'none';
    document.getElementById('snowflakes').style.display = 'none';
    var uno = document.getElementById('uno');
    if ( uno.hasChildNodes() )
    {
        while ( uno.childNodes.length >= 1 )
        {
            uno.removeChild( uno.firstChild );
        }
    }
    uno.innerHTML='<p id="unop">UNO</p>';
    document.getElementById("unop").style.visibility = 'visible';
    var dos = document.getElementById('dos');
    if ( dos.hasChildNodes() )
    {
        while ( dos.childNodes.length >= 1 )
        {
            dos.removeChild( dos.firstChild );
        }
    }
    dos.innerHTML='<p id="dosp">DOS</p>';
    document.getElementById("dosp").style.visibility = 'visible';
    var tres = document.getElementById('tres');
    if ( tres.hasChildNodes() )
    {
        while ( tres.childNodes.length >= 1 )
        {
            tres.removeChild( tres.firstChild );
        }
    }
    tres.innerHTML='<p id="tresp">TRES</p>';
    document.getElementById("tresp").style.visibility = 'visible';
    var cuatro = document.getElementById('cuatro');
    if ( cuatro.hasChildNodes() )
    {
        while ( cuatro.childNodes.length >= 1 )
        {
            cuatro.removeChild( cuatro.firstChild );
        }
    }
    cuatro.innerHTML='<p id="cuatrop">CUATRO</p>';
    document.getElementById("cuatrop").style.visibility = 'visible';
    var cinco = document.getElementById('cinco');
    if ( cinco.hasChildNodes() )
    {
        while ( cinco.childNodes.length >= 1 )
        {
            cinco.removeChild( cinco.firstChild );
        }
    }
    cinco.innerHTML='<p id="cincop">CINCO</p>';
    document.getElementById("cincop").style.visibility = 'visible';
    var seis = document.getElementById('seis');
    if ( seis.hasChildNodes() )
    {
        while ( seis.childNodes.length >= 1 )
        {
            seis.removeChild( seis.firstChild );
        }
    }
    seis.innerHTML='<p id="seisp">SEIS</p>';
    document.getElementById("seisp").style.visibility = 'visible';
    var siete = document.getElementById('siete');
    if ( siete.hasChildNodes() )
    {
        while ( siete.childNodes.length >= 1 )
        {
            siete.removeChild( siete.firstChild );
        }
    }
    siete.innerHTML='<p id="sietep">SIETE</p>';
    document.getElementById("sietep").style.visibility = 'visible';
    var ocho = document.getElementById('ocho');
    if ( ocho.hasChildNodes() )
    {
        while ( ocho.childNodes.length >= 1 )
        {
            ocho.removeChild( ocho.firstChild );
        }
    }
    ocho.innerHTML='<p id="ochop">OCHO</p>';
    document.getElementById("ochop").style.visibility = 'visible';
    var nueve = document.getElementById('nueve');
    if ( nueve.hasChildNodes() )
    {
        while ( nueve.childNodes.length >= 1 )
        {
            nueve.removeChild( nueve.firstChild );
        }
    }
    nueve.innerHTML='<p id="nuevep">NUEVE</p>';
    document.getElementById("nuevep").style.visibility = 'visible';
}

function guardar(){

    var nombreUsuario = document.getElementById('nombre');

    if(nombreUsuario.innerHTML == ''){
        alert('Tienes que inciar sesión');
    }else{
        localStorage.removeItem(nombreUsuario.innerHTML);
        localStorage.removeItem(nombreUsuario.innerHTML + 'Background');
        var body = document.getElementById('body');
        localStorage.setItem(nombreUsuario.innerHTML, body.innerHTML);
        localStorage.setItem(nombreUsuario.innerHTML + 'Background', document.getElementById('body').style.backgroundImage);
        alert('Configuración guardada ' + nombreUsuario.innerHTML);
    }
}

function titulo(){
    var pos = document.getElementById('escPosicion').value;
    var fills = document.getElementById(pos).children;
    if(fills.length > 0){
        fills[0].remove();
    }
    var x = document.getElementById("titulo").value;
    var fuente = document.getElementById("escFuente").value;
    var titulo = document.createElement("span");
    titulo.setAttribute("id", 'title');
    titulo.classList.add(fuente);
    titulo.style.color = document.getElementById('colorEscoger').value;
    titulo.innerHTML = x.toUpperCase();
    document.getElementById(pos).appendChild(titulo);
}

function aplicarFondo() {
    var value = document.getElementById('escImagen').value;
    document.getElementById('body').style.backgroundImage = "url('../imagenes/" + value + ".jpg')";
}

function mostrarNoticias(){
    var ver = document.getElementById("escNoticias").value;
    if(ver == 'si'){
        var divParent = document.getElementById('tres');
        divParent.innerHTML =
            '    <div class="eventos">\n' +
            '        <div class="eventos--inner">\n' +
            '            <marquee scrollamount="20" id="marquee"  onmouseout="this.start()" onmouseover="this.stop()"></marquee>\n' +
            '        </div>\n' +
            '    </div>';
        var x = document.getElementById("contenidoNoticias").value;
        var marquee = document.getElementById('marquee');
        marquee.innerHTML = x.toString().toUpperCase();
    }else{
        var divParent = document.getElementById('tres');
        var hijos = document.getElementById('tres').children;
        if(!divParent.contains(document.getElementById('title'))){
            hijos[0].remove();
        }

    }
}



function mostrarEstaciones(){

    pos = document.getElementById('escPosicionEstaciones').value;
    divParent = document.getElementById(pos);

    let int = parseInt(divParent.getAttribute('value'));
        divParent.innerHTML =
            '<button class="myButton" onclick="plusDivs(-1, '+ int +' )">&#10094;</button>' +
            '<img class="imgDiv'+int+'" id="mySlides" src="../imagenes/verano.jpg">' +
            '<img class="imgDiv'+int+'" id="mySlides" src="../imagenes/otoño.jpg">' +
            '<img class="imgDiv'+int+'" id="mySlides" src="../imagenes/invierno.jpg">' +
            '<img class="imgDiv'+int+'" id="mySlides" src="../imagenes/primavera.jpg">' +
            '<button class="myButton" onclick="plusDivs(1,  '+ int +' )">&#10095;</button>';
        ;

        indexEst[int] = 1;
        showDivs(indexEst[int], int);



}

function mostrarConstelacion() {

    pos = document.getElementById('escPosicionConstelacion').value;
    divParent = document.getElementById(pos);

    divParent.innerHTML = '<img id="constelacion" src="../imagenes/constelacion.gif">';
}

function plusDivs(n, valor) {

    showDivs(indexEst[valor] += n, valor);
}

function showDivs(n, valor) {

    var i;
    var x = document.getElementsByClassName('imgDiv'+valor);
    if (n > x.length) {indexEst[valor] = 1}
    if (n < 1) {indexEst[valor] = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[indexEst[valor]-1].style.display = "block";
}


function activarCara(){
    var active = document.getElementById('escCara').value;
    if(active == 'si'){
        var cara = document.getElementById('container');
        cara.removeAttribute('style');
    }else{
        var cara = document.getElementById('container');
        cara.style.display = 'none';
    }
}

function caer(){
    var caer = document.getElementById('escCaer').value;
    if(caer == 'nieve'){
        var nieve = document.getElementById('snowflakes');
        nieve.style.display = 'block';
    }else if(caer == 'hojas'){
        var hojas = document.getElementById('hojas');
        hojas.style.display = 'block';
    }else if(caer == 'lluvia'){
        var lluvia = document.getElementById('rain');
        lluvia.style.display = 'block';
        createRain();
    }
}
