var ctx = document.getElementById("myChart").getContext("2d");
var myChart = new Chart(ctx, {
    type: "pie",
    data: {
        labels: ['agricultores', 'cazadores', 'guerreros', 'pescadores', 'jefes'],
        datasets: [{
            label: 'Num datos',
            data: [5, 5, 5, 5, 1],
            backgroundColor: [
                'rgb(66, 134, 244,0.8)',
                'rgb(74, 135, 72,0.8)',
                'rgb(229, 89, 50,0.8)',
                'rgb(255, 255, 0,0.8)',
                'rgb(255, 140, 0,0.8)'
            ]
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById("barChart").getContext("2d");
var barChart = new Chart(ctx, {
    type: "bar",
    data: {
        labels: ['Raznarok', 'Boris', 'Cepalek', 'Cimbeleta', 'Curkelet', 'Quirembe', 'Estirla', 'Pelaif', 'Zimbateua', 'Permila', 'Sarambui', 'Delfi', 'Xervilon', 'Hyiol', 'Arpolinteri', 'Guiop', 'Vermienta', 'Feripedalo', 'Zughi', 'Terpatoca', 'Collins'],
        datasets: [{
            label: 'Edad',
            data: [27, 43, 20, 26, 16, 24, 19, 34, 27, 20, 18, 25, 36, 32, 21, 34, 23, 28, 38, 40, 60],
            backgroundColor: [
                'rgb(242,163,94)',
                'rgb(89,56,28)',
                'rgb(140,39,3)',
                'rgb(234,147,82)',
                'rgba(141,91,37,0.8)',
                'rgb(242,163,94)',
                'rgb(89,56,28)',
                'rgb(140,39,3)',
                'rgb(234,147,82)',
                'rgba(141,91,37,0.8)',
                'rgb(242,163,94)',
                'rgb(89,56,28)',
                'rgb(140,39,3)',
                'rgb(234,147,82)',
                'rgba(141,91,37,0.8)',
                'rgb(242,163,94)',
                'rgb(89,56,28)',
                'rgb(140,39,3)',
                'rgb(234,147,82)',
                'rgba(141,91,37,0.8)',
                'rgb(89,56,28)',
            ]
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});