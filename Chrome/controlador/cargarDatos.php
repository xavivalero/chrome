<?php
session_start();

$fileSQL = file_get_contents('../model/scriptBBDD/interficies.sql');
$servername = "localhost:3307";
$username = "root";
$password = "super3";
$conn = mysqli_connect($servername, $username, $password) or die("Conexion fallida: " . mysqli_connect_error());

/* Ejecutar consulta multiquery */
if ($conn->multi_query($fileSQL)) {
    do {
        /* Almacenar primer juego de resultados */
        if ($result = $conn->store_result()) {
            while ($row = $result->fetch_assoc()) {
                print_r($row);
                echo "<br/>";
            }
            $result->free();
        }
        /* mostrar divisor */
        if ($conn->more_results()) {
            printf("-----------------\n");
        }
        // Avanzar al siguiente resultado
    } while ($conn->next_result());
}
$_SESSION['cargarDatos'] = true;

$direc = "../vista/home.php";
header('Location:'.$direc);

/* cerrar conexión */
$conn->close();

?>