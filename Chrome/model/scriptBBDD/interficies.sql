-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2022 at 03:10 PM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `interficies`
--
CREATE DATABASE IF NOT EXISTS `interficies` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `interficies`;

--

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
                            `id` int(11) NOT NULL,
                            `nombre` varchar(250) NOT NULL,
                            `edad` int(11) NOT NULL,
                            `salario` float NOT NULL,
                            `mail` varchar(250) NOT NULL,
                            `contraseña` varchar(250) NOT NULL,
                            `id_cargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `edad`, `salario`, `mail`, `contraseña`, `id_cargo`) VALUES
                                                                                                 (1, 'raznarok', 27, 0, 'raznarok@unga.com', 'raznarok', 1),
                                                                                                 (2, 'boris', 43, 0, 'boris@unga.com', 'boris', 3),
                                                                                                 (3, 'cepalek', 20, 0, 'cepalek@unga.com', 'cepalek', 1),
                                                                                                 (4, 'cimbeleta', 26, 0, 'cimbeleta@unga.com', 'cimbeleta', 1),
                                                                                                 (5, 'curkelet', 16, 0, 'curkelet@unga.com', 'curkelet', 1),
                                                                                                 (6, 'quirembe', 24, 0, 'quirembe@unga.com', 'quirembe', 1),
                                                                                                 (7, 'estirla', 19, 0, 'estirla@unga.com', 'estirla', 2),
                                                                                                 (8, 'pelaif', 34, 0, 'pelaif@unga.com', 'pelaif', 2),
                                                                                                 (9, 'zimbateua', 27, 0, 'zimbateua@unga.com', 'zimbateua', 3),
                                                                                                 (10, 'permila', 20, 0, 'permila@unga.com', 'permila', 2),
                                                                                                 (11, 'sarambui', 18, 0, 'sarambui@unga.com', 'sarambui', 4),
                                                                                                 (12, 'delfi', 25, 0, 'delfi@unga.com', 'delfi', 4),
                                                                                                 (13, 'xervilon', 36, 0, 'xervilon@unga.com', 'xervilon', 4),
                                                                                                 (14, 'hyiol', 32, 0, 'hyiol@unga.com', 'hyiol', 4),
                                                                                                 (15, 'arpolinteri', 21, 0, 'arpolinteri@outlook.com', 'arpolinteri', 4),
                                                                                                 (16, 'guiop', 34, 0, 'guiop@unga.com', 'guiop', 2),
                                                                                                 (17, 'vermienta', 23, 0, 'vermienta@unga.com', 'vermienta', 2),
                                                                                                 (18, 'feripedalo', 28, 0, 'feripedalo@unga.com', 'feripedalo', 3),
                                                                                                 (19, 'zughi', 38, 0, 'zughi@unga.com', 'zughi', 3),
                                                                                                 (20, 'terpatoca', 40, 0, 'terpatoca@unga.com', 'terpatoca', 3),
                                                                                                 (21, 'collins', 60, 0, 'collins@unga.com', 'collins', 5),
                                                                                                 (22, 'Victor', 19, -100, 'victor@unga.com', 'victor', 5),
                                                                                                 (23, 'Xavi', 19, 5, 'xavi@unga.com', 'xavi', 5),
                                                                                                 (24, 'Marc', 19, 2000000, 'marc@unga.com', 'marc', 5);
-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
CREATE TABLE `cargos` (
                          `id` int(11) NOT NULL,
                          `descripcion` varchar(250) DEFAULT NULL,
                          `fotoConExtension` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargos`
--

INSERT INTO `cargos` (`id`, `descripcion`, `fotoConExtension`) VALUES
                                                                   (1, 'recolector', 'recolector.png'),
                                                                   (2, 'cazador', 'cazador.png'),
                                                                   (3, 'guerrero', 'guerrero.png'),
                                                                   (4, 'pescador', 'pescador.png'),
                                                                   (5, 'jefe', 'jefe.png');
DROP TABLE IF EXISTS `tutoriales`;
CREATE TABLE `tutoriales` (
                          `id` int(11) NOT NULL,
                          `titulo` varchar(250) DEFAULT NULL,
                          `descripcion` varchar(250) DEFAULT NULL,
                          `video` varchar(250) NOT NULL,
                          `tipo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargos`
--

INSERT INTO `tutoriales` (`id`, `titulo`, `descripcion`, `video`, `tipo`) VALUES
                                                                   (1,'¿Como hacer fuego y para que sirve?', 'En este video enseñamos de qué maneras se puede utilizar el fuego y en que situaciones nos puede ser de utilidad.', 'fuego', 'tutoriales'),
                                                                   (2,'¿Como hacer fuego y para que sirve?', 'En este vídeo enseñamos de qué máneras se puede utilizar el fuego y en qué situaciones nos puede ser de utilidad.', 'fuego', 'fuego'),
                                                                   (3,'Aprende a cazar', 'En este vídeo enseñamos un vídeo muy amplio que trata sobre la caza del mamut', 'caza', 'caza'),
                                                                   (4,'Aprende a cazar', 'En este vídeo enseñamos un vídeo muy amplio que trata sobre la caza del mamut', 'caza', 'tutoriales'),
                                                                   (5,'Video de presentacion', 'En este vídeo, el qual forma parte de nuestra última entrega de esta asignatura, mostramos de una forma resumida, nuestro trabajo al largo del año.', 'presentacion', 'web');
--
-- Indexes for dumped tables
--

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
    ADD PRIMARY KEY (`id`),
    ADD KEY `id_cargo` (`id_cargo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
    ADD CONSTRAINT `id_cargo` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
